# IntraCAns

## Prérequis
La version de python utilisée est [python-3.8.0](https://www.python.org/downloads/)

## Environnement virtuel Python
La création de l'environnement virtuel python n'est pas obligatoire mais conseillée.

L'environnement virtuel va permettre d'isoler les dépendances python spécifiques au projet évitant ainsi
les problèmes de version entre les packages installés sur votre machine.

### Création de l'environnement  virtuel
Pour créer l'environnement, placez vous à la racine des sources du repository et lancez les commandes suivantes :
```
mkdir intracans-virtualenv
python -m venv intracans-virtualenv
```

### Lancement de l'environnement virtuel
Pour lancer l'environnement lancez une des commandes suivantes :

- Sous Unix : `source intracans-virtualenv/bin/activate`
- Sous Windows
  - Git Bash : `source intracans-virtualenv/Scripts/activate`
  - CMD : `intracans-virtualenv\Scripts\activate.bat`
  - PowerShell : `intracans-virtualenv/Scripts/activate.ps1`

Plus d'information sur venv : https://docs.python.org/3/library/venv.html

### Installation des dépendances
Une fois l'environnement virtuel lancé, il faut installer les dépendances. Pour ce faire, lancer la commande suivante à la racine du repository :
```
pip install -r requirements.txt
```

## Lancement de l'application
Vous pouvez lancer l'application de deux manière différentes.

### Serveur Django
Django met à dispostion un serveur intégré au framework pour lancer les applications en local.

Lancez les commandes suivantes à la racine du repository :
```
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
```

Vous pourrez accéder à l'API à l'URL suivante : http://localhost:8000/