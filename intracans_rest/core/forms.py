from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import AcensiUser


class AcensiUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm):
        model = AcensiUser
        fields = ('email', 'firstname', 'lastname')


class AcensiUserChangeForm(UserChangeForm):

    class Meta:
        model = AcensiUser
        fields = ('email', 'firstname', 'lastname')