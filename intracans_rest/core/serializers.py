from rest_framework import serializers
from .models import Contract
from .models import AcensiUser

class AcensiUserSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = AcensiUser
        fields = ['url', 'id', 'firstname', 'lastname', 'date_joined', 'is_active', 'is_superuser']


class ContractSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Contract
        fields = ['url', 'id', 'code', 'description']