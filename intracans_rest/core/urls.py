from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token
from .views import ApiRoot, UserDetail, UserList, ContractDetail, ContractList

urlpatterns = [
    # API ROOT
    path('', ApiRoot.as_view(), name='api-root'),

    # PROFILES
    path('users/', UserList.as_view(), name='acensiuser-list'),
    path('users/<uuid:pk>/', UserDetail.as_view(), name='acensiuser-detail'),

    # CONTRACTS
    path('contracts/', ContractList.as_view(), name='contract-list'),
    path('contracts/<uuid:pk>/', ContractDetail.as_view(), name='contract-detail'),

    # AUTHENTICATION
    path('register/', obtain_auth_token, name='api_token_auth')
]