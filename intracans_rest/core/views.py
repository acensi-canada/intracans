from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from django.http import Http404

from .models import Contract, AcensiUser

from .serializers import AcensiUserSerializer, ContractSerializer

class ApiRoot(APIView):
    def get(self, request):
        return Response({
            'users': reverse('acensiuser-list', request=request),
            'contracts': reverse('contract-list', request=request)
        })

class UserList(APIView): 
    def get(self, request):
        users = AcensiUser.objects.all()
        serializer = AcensiUserSerializer(users, context={'request': request}, many=True)
        return Response(serializer.data)

    def post(self):
        pass

class UserDetail(APIView):

    def get_object(self, pk):
        try:
            return AcensiUser.objects.get(pk=pk)
        except AcensiUser.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        user = self.get_object(pk)
        serializer = AcensiUserSerializer(user, context={'request': request})
        return Response(serializer.data)

class ContractList(APIView): 
    def get(self, request):
        contracts = Contract.objects.all()
        serializer = ContractSerializer(contracts, context={'request': request}, many=True)
        return Response(serializer.data)

    def post(self):
        pass

class ContractDetail(APIView):

    def get_object(self, pk):
        try:
            return Contract.objects.get(pk=pk)
        except Contract.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        contract = self.get_object(pk)
        serializer = ContractSerializer(contract, context={'request': request})
        return Response(serializer.data)
