from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import AcensiUserChangeForm, AcensiUserCreationForm
from .models import Contract, AcensiUser

 
class AcensiUserAdmin(UserAdmin):
    add_form = AcensiUserCreationForm
    form = AcensiUserChangeForm
    model = AcensiUser
    list_display = ('email', 'firstname', 'lastname', 'is_staff', 'is_active',)
    list_filter = ('email', 'firstname', 'lastname', 'is_staff', 'is_active',)
    fieldsets = (
        (None, {'fields': ('email', 'firstname', 'lastname', 'password')}),
        ('Permissions', {'fields': ('is_staff', 'is_active')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'firstname', 'lastname', 'password1', 'password2', 'is_staff', 'is_active')}
        ),
    )
    search_fields = ('email', 'firstname', 'lastname')
    ordering = ('email', 'firstname', 'lastname')


admin.site.register(AcensiUser, AcensiUserAdmin)
admin.site.register(Contract)
