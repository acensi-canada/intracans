from django.contrib import admin
from django.urls import path, include
from core.views import ApiRoot

urlpatterns = [

    # ROOT
    path('', ApiRoot.as_view(), name='root'),

    # ADMIN
    path('admin/', admin.site.urls),

    # CORE
    path('api/v1/', include('core.urls'))
]
